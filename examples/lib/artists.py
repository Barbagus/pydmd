from __future__ import annotations

from typing import Any as any
from typing import cast

from . import artist as _artist

def _as_list(chain: any):
    def inner(data: any):
        return [chain(e) for e in data]

    return inner

def _is_list(chain: any):
    def inner(data: any, path: str):
        if not isinstance(data, list):
            raise TypeError(f'{path or "/"} not a list: {type(data)}')

        for i, e in enumerate(cast(list[any], data)):
            chain(e, path + '/' + str(i))

    return inner


Artists = list[_artist.Artist]

def artists__validate_json(data: any, path: str=''):
    _is_list(_artist.Artist.validate_json)(data, path)

def artists__from_json(data: any) -> Artists:
    return _as_list(_artist.Artist.from_json)(data)

