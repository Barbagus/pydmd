from __future__ import annotations

from dataclasses import dataclass
from typing import Any as any
from typing import Literal as literal
from typing import Optional as optional
from typing import cast

def _as_list(chain: any):
    def inner(data: any):
        return [chain(e) for e in data]

    return inner

def _is_enum(items: list[str]):
    def inner(data: any, path: str):
        if not isinstance(data, str):
            raise TypeError(f'{path or "/"} not a string: {type(data)}')

        if data not in items:
            raise ValueError(f'{path or "/"} invalid value: {data!r}')

    return inner

def _is_list(chain: any):
    def inner(data: any, path: str):
        if not isinstance(data, list):
            raise TypeError(f'{path or "/"} not a list: {type(data)}')

        for i, e in enumerate(cast(list[any], data)):
            chain(e, path + '/' + str(i))

    return inner

def _is_mapping(data: any, fields: set[str], path: str):
    if not isinstance(data, dict):
        raise TypeError(f'{path or "/"} not a mapping: {type(data)}')

    data = cast(dict[str, any], data)
    keys = set(data)
    if not keys.issubset(fields):
        raise ValueError(f'{path or "/"} extra members: {keys - fields}')

def _is_optional(chain: any):
    def inner(data: any, path: str):
        if data is not None:
            chain(data, path)

    return inner

def _is_scalar(scalar: any, name: str):
    def inner(data: any, path: str):
        if not isinstance(data, scalar):
            raise TypeError(f'{path or "/"} not a {name}: {type(data)}')

    return inner


@dataclass
class ArtistAlbumsItem:
    name: str
    year: int

    @classmethod
    def validate_json(cls, data: any, path: str=''):
        _is_mapping(data, {'year', 'name'}, path)
        _is_scalar(str, 'string')(
            data['name'],
            path + '/name'
        )
        _is_scalar(int, 'integer')(
            data['year'],
            path + '/year'
        )

    @classmethod
    def from_json(cls, data: any):
        return cls(
            data['name'],
            data['year'],
        )

@dataclass
class Artist:
    albums: list[ArtistAlbumsItem]
    awards: optional[list[Award]]
    name: str
    style: literal['POP', 'ROCK', 'RAP', 'OTHER']
    wikipedia_page: optional[str]

    @classmethod
    def validate_json(cls, data: any, path: str=''):
        _is_mapping(data, {'albums', 'style', 'awards', 'name', 'wikipediaPage'}, path)
        _is_list(ArtistAlbumsItem.validate_json)(
            data['albums'],
            path + '/albums'
        )
        _is_optional(_is_list(award__validate_json))(
            data.get('awards'),
            path + '/awards'
        )
        _is_scalar(str, 'string')(
            data['name'],
            path + '/name'
        )
        _is_enum(['POP', 'ROCK', 'RAP', 'OTHER'])(
            data['style'],
            path + '/style'
        )
        _is_optional(_is_scalar(str, 'string'))(
            data.get('wikipediaPage'),
            path + '/wikipediaPage'
        )

    @classmethod
    def from_json(cls, data: any):
        return cls(
            _as_list(ArtistAlbumsItem.from_json)(data['albums']),
            data.get('awards'),
            data['name'],
            data['style'],
            data.get('wikipediaPage'),
        )

Award = literal['MTV', 'GOLD_RECORD', 'PLATINUM_RECORD']

def award__validate_json(data: any, path: str=''):
    _is_enum(['MTV', 'GOLD_RECORD', 'PLATINUM_RECORD'])(data, path)

def award__from_json(data: any) -> Award:
    return data

