from pathlib import Path

import yaml
from dmd import compile_dmds

DMDS_DIR = Path(__file__).parent.joinpath('type_defs')
LIB_DIR = Path(__file__).parent.joinpath('lib')


compile_dmds(
    [
        (dmd_file.stem, yaml.safe_load(dmd_file.read_text()))
        for dmd_file in DMDS_DIR.rglob('*.yml')
    ],
    LIB_DIR
)
