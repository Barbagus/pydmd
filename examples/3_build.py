import json
from pathlib import Path

from lib.artists import artists__from_json

DATA_FILE = Path(__file__).parent.joinpath('data/db.json')

DATA = json.loads(DATA_FILE.read_text())

artists = artists__from_json(DATA)
