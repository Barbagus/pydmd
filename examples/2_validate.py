import json
from pathlib import Path

from lib.artists import artists__validate_json

DATA_FILE = Path(__file__).parent.joinpath('data/db.json')

DATA = json.loads(DATA_FILE.read_text())

artists__validate_json(DATA)
