# Copyright 2021 Etienne Zind
#
# This file is part of PyDMD.
#
# PyDMD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDMD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDMD.  If not, see <https://www.gnu.org/licenses/>.

"""
PyDMD (python declarative module definition) -- tree elements module
"""

from __future__ import annotations

from dataclasses import dataclass
from typing import Annotated, Literal, Optional, Union
from collections.abc import Callable

@dataclass
class ArrayOf:
    """List of element of the same type."""

    elements: Tree

@dataclass
class ClassField:
    flat: str
    tree: Tree
    name: str

@dataclass
class ClassFields:
    """Class definition fields."""

    fields: list[ClassField]

@dataclass
class EnumValue:
    """String enumeration."""

    items: list[str]

@dataclass
class NullableValue:
    """Nullable value."""

    inner: Tree

@dataclass
class OptionalValue:
    """Optional value."""

    inner: Tree

@dataclass
class Reference:
    """Reference."""

    symbol: Symbol
    """Target symbol."""

    module_name: Optional[str]
    """Target module name, or None if reference within current module."""

    tree: Optional[Tree]
    """Target tree, once linked."""

ScalarType = Annotated[
    Literal['int', 'float', 'bool', 'str'],
    "Scalar types."
]

@dataclass
class ScalarValue:
    """Scalar type."""

    type: ScalarType

    _NAME = {
        'int': 'integer',
        'float': 'float',
        'bool': 'boolean',
        'str': 'string',
    }

    @property
    def name(self):
        """The type's common name."""
        return self._NAME[self.type]

Tree = Annotated[
    Union[
        ArrayOf,
        ClassFields,
        EnumValue,
        NullableValue,
        OptionalValue,
        Reference,
        ScalarValue,
    ],
    "General tree type."
]

Symbol = Annotated[
    str, # should be class name formated as PEP 8
    "A entry name in a type definition collection."
]

def visit_tree(tree: Tree, visitor: Callable[[Tree], None]):
    """
    A depth-first tree visitor
    """

    if isinstance(tree, (OptionalValue, NullableValue)):
        visit_tree(tree.inner, visitor)

    elif isinstance(tree, ArrayOf):
        visit_tree(tree.elements, visitor)

    elif isinstance(tree, ClassFields):
        for field in tree.fields:
            visit_tree(field.tree, visitor)

    return visitor(tree)
