# Copyright 2021 Etienne Zind
#
# This file is part of PyDMD.
#
# PyDMD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDMD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDMD.  If not, see <https://www.gnu.org/licenses/>.

"""
PyDMD (python declarative module definition) -- reference linking module
"""
from collections.abc import Mapping

from .tree import Reference, Symbol, Tree, visit_tree
from .exceptions import DMDLinkException


def link_modules(modules: Mapping[str, Mapping[Symbol, Tree]]):
    """Resolve inline and external references."""

    for module_name, module in modules.items():

        def visitor(tree: Tree):
            if isinstance(tree, Reference):
                if tree.module_name is None:
                    try:
                        tree.tree = module[tree.symbol]
                    except KeyError:
                        raise DMDLinkException(f'Inline reference not found in {module_name!r}: {tree.symbol!r}')
                else:
                    try:
                        tree.tree = modules[tree.module_name][tree.symbol]
                    except KeyError:
                        raise DMDLinkException(f'External reference not found in {module_name!r}: \'{tree.module_name}.{tree.symbol}\'')

        for tree in module.values():
            visit_tree(tree, visitor)
