# Copyright 2021 Etienne Zind
#
# This file is part of PyDMD.
#
# PyDMD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDMD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDMD.  If not, see <https://www.gnu.org/licenses/>.

"""
PyDMD (python declarative module definition) -- main code generation module
"""
from collections.abc import Callable, Mapping
from dataclasses import dataclass, field
from inspect import getsource
from io import StringIO
from typing import Any, Optional

from .exceptions import DMDException
from .helpers import (as_list, as_nullable, as_optional, is_enum, is_list, is_mapping,
                      is_nullable, is_optional, is_scalar)
from .names import to_flat_name
from .tree import (ArrayOf, ClassField, ClassFields, EnumValue, NullableValue,
                   OptionalValue, Reference, ScalarValue, Symbol, Tree,
                   visit_tree)

_T_ANY = 'any'
_T_LITERAL = 'literal'
_T_OPTIONAL = 'optional'
_TAB = ' ' * 4


@dataclass
class _Writer:

    _helpers: set[Callable[..., Any]] = field(init=False, default_factory=set)

    _dataclasses_import: set[str] = field(init=False, default_factory=set)

    _typing_imports: set[str] = field(init=False, default_factory=set)

    _external_imports: set[str] = field(init=False, default_factory=set)

    _body: StringIO = field(init=False, default_factory=StringIO)

    #
    # Header section
    #
    def _use_dataclass(self):
        self._dataclasses_import.add('dataclass')

    def _use_any(self):
        self._typing_imports.add(f'Any as {_T_ANY}')

    def _use_literal(self):
        self._typing_imports.add(f'Literal as {_T_LITERAL}')

    def _use_optional(self):
        self._typing_imports.add(f'Optional as {_T_OPTIONAL}')

    def _use_cast(self):
        self._typing_imports.add(f'cast')

    def _use_helper(self, fun: Callable[..., Any]):
        self._helpers.add(fun)

    def _use_external(self, module_name: str):
        self._external_imports.add(module_name)

    def _find_externals(self, tree: Tree):
        # finds and "imports" external modules

        def visitor(tree: Tree):
            if isinstance(tree, Reference) and tree.module_name is not None:
                self._use_external(tree.module_name)

        visit_tree(tree, visitor)

    def _dump_header(self):
        out = StringIO()
        # always imported
        out.write('from __future__ import annotations\n\n')

        # dataclass import
        for s in sorted(self._dataclasses_import):
            out.write(f'from dataclasses import {s}\n')

        # typing import
        for s in sorted(self._typing_imports):
            out.write(f'from typing import {s}\n')

        if self._dataclasses_import or self._typing_imports:
            out.write('\n')

        # external imports
        for module_name in sorted(self._external_imports):
            out.write(f'from . import {module_name} as _{module_name}\n')

        if self._external_imports:
            out.write('\n')

        # helper functions
        for fun in sorted(self._helpers, key=lambda f: f.__name__):
            out.write(getsource(fun))
            out.write('\n')

        return out.getvalue()

    #
    # Type hinting
    #
    def _type_hint_expression(self, tree: Tree) -> str:
        # Recursively translate a tree into a type hint expression.

        if isinstance(tree, ArrayOf):
            type_hint = self._type_hint_expression(tree.elements)
            return f'list[{type_hint}]'

        if isinstance(tree, EnumValue):
            self._use_literal()
            return f'{_T_LITERAL}{tree.items!r}'

        if isinstance(tree, Reference):
            if tree.module_name is None:
                return tree.symbol

            return f'_{tree.module_name}.{tree.symbol}'

        if isinstance(tree, (NullableValue, OptionalValue)):
            # as far as type hinting is concerned, nullable or optional or both is
            # the same
            type_hint = self._type_hint_expression(tree.inner)
            if type_hint.startswith(f'{_T_OPTIONAL}['):
                return type_hint

            self._use_optional()
            return f'{_T_OPTIONAL}[{type_hint}]'

        if isinstance(tree, ScalarValue):
            return tree.type

        raise DMDException(f'Unexpected')

    #
    # Validation expression
    #
    def _validator_chain(self, module_name: Optional[str], tree: Tree) -> str:
        # Recursively translate a tree into a validation call chain.

        if isinstance(tree, Reference):
            if tree.tree is None:
                raise DMDException(f'Unexpected.')

            if isinstance(tree.tree, ClassFields):
                call = f'{tree.symbol}.validate_json'

            else:
                call = f'{to_flat_name(tree.symbol)}__validate_json'

            if tree.module_name is not None:
                # external class def
                call = f'_{tree.module_name}.{call}'

            return call

        if isinstance(tree, ArrayOf):
            args = self._validator_chain(module_name, tree.elements)

            self._use_any()
            self._use_cast()
            self._use_helper(is_list)
            return f'_is_list({args})'

        if isinstance(tree, EnumValue):
            args = repr(tree.items)

            self._use_any()
            self._use_helper(is_enum)
            return f'_is_enum({args})'

        if isinstance(tree, NullableValue):
            args = self._validator_chain(module_name, tree.inner)

            self._use_any()
            self._use_helper(is_nullable)
            return f'_is_nullable({args})'

        if isinstance(tree, OptionalValue):
            args = self._validator_chain(module_name, tree.inner)

            self._use_any()
            self._use_helper(is_optional)
            return f'_is_optional({args})'

        if isinstance(tree, ScalarValue):
            args = f'{tree.type}, {tree.name!r}'

            self._use_any()
            self._use_helper(is_scalar)
            return f'_is_scalar({args})'

        raise DMDException(f'Unexpected')

    #
    # Initialization expression
    #
    def _initialization_chain(self, module_name: Optional[str], tree: Tree) -> Optional[str]:
        # Recursively translate a tree into a initialization call chain.
        #
        # We do something clever here collapsing the initialization chain if the end "link" is
        # not a object construction.
        #
        # Assuming the input is valid, only object construction need some processing at
        # initialization time, other values (scalar, enums, lists, etc...) are compatible
        # with the input value and therefore need no processing.
        #
        # To do this we must visit the tree accros external references... to really get to the bottom
        # of the chain.

        if isinstance(tree, Reference):
            if tree.tree is None:
                raise DMDException(f'Unexpected.')

            if isinstance(tree.tree, ClassFields):
                call = f'{tree.symbol}.from_json'
                if tree.module_name is not None:
                    # external class def
                    call = f'_{tree.module_name}.{call}'

                # only time we actually return something
                return call

            if tree.module_name is not None:
                # traversing accross external module
                return self._initialization_chain(tree.module_name, tree.tree)

            return self._initialization_chain(module_name, tree.tree)

        if isinstance(tree, ArrayOf):
            args = self._initialization_chain(module_name, tree.elements)
            if args is None:
                return None

            self._use_any()
            self._use_helper(as_list)
            return f'_as_list({args})'

        if isinstance(tree, (EnumValue, ScalarValue)):
            return None

        if isinstance(tree, NullableValue):
            args = self._initialization_chain(module_name, tree.inner)
            if args is None:
                return None

            self._use_any()
            self._use_helper(as_nullable)
            return f'_as_nullable({args})'

        if isinstance(tree, OptionalValue):
            args = self._initialization_chain(module_name, tree.inner)
            if args is None:
                return None

            self._use_any()
            self._use_helper(as_optional)
            return f'_as_optional({args})'

        raise DMDException(f'Unexpected')

    #
    # Write class definition
    #
    def _write_class_fields(self, fields: list[ClassField]):
        for f in fields:
            type_hint = self._type_hint_expression(f.tree)
            self._body.write(f'{_TAB}{f.flat}: {type_hint}\n')

    def _write_class_field_validator(self, field: ClassField):
        validator = self._validator_chain(None, field.tree)

        getter = f'data.get({field.name!r})' if isinstance(field.tree, OptionalValue) else f'data[{field.name!r}]'

        xpath = f'/{field.name}'

        self._body.write(f'{_TAB * 2}{validator}(\n')
        self._body.write(f'{_TAB * 3}{getter},\n')
        self._body.write(f'{_TAB * 3}path + {xpath!r}\n')
        self._body.write(f'{_TAB * 2})\n')

    def _write_class_validator(self, fields: list[ClassField]):
        self._use_any()
        self._use_cast()
        self._use_helper(is_mapping)

        field_set = {f.name for f in fields}

        self._body.write(f'{_TAB}@classmethod\n')
        self._body.write(f'{_TAB}def validate_json(cls, data: {_T_ANY}, path: str=\'\'):\n')
        self._body.write(f'{_TAB * 2}_is_mapping(data, {field_set!r}, path)\n')
        for field in fields:
            self._write_class_field_validator(field)

    def _write_class_field_initializer(self, field: ClassField):
        initializer = self._initialization_chain(None, field.tree)

        getter = f'data.get({field.name!r})' if isinstance(field.tree, OptionalValue) else f'data[{field.name!r}]'

        if initializer:
            self._body.write(f'{_TAB * 3}{initializer}({getter}),\n')

        else:
            self._body.write(f'{_TAB * 3}{getter},\n')

    def _write_class_initializer(self, fields: list[ClassField]):
        self._use_any()

        self._body.write(f'{_TAB}@classmethod\n')
        self._body.write(f'{_TAB}def from_json(cls, data: {_T_ANY}):\n')
        self._body.write(f'{_TAB * 2}return cls(\n')
        for field in fields:
            self._write_class_field_initializer(field)

        self._body.write(f'{_TAB * 2})\n')

    def write_class_definition(self, symbol: str, fields: list[ClassField]):
        """Write a class definition."""

        for field in fields:
            self._find_externals(field.tree)

        self._use_dataclass()

        self._body.write(f'@dataclass\n')
        self._body.write(f'class {symbol}:\n')

        self._write_class_fields(fields)
        self._body.write(f'\n')

        self._write_class_validator(fields)
        self._body.write(f'\n')

        self._write_class_initializer(fields)
        self._body.write(f'\n')

    #
    # Write type alias
    #
    def _write_type_alias_validator(self, symbol: Symbol, tree: Tree):
        validator = self._validator_chain(None, tree)

        self._body.write(f'def {to_flat_name(symbol)}__validate_json(data: {_T_ANY}, path: str=\'\'):\n')
        self._body.write(f'{_TAB}{validator}(data, path)\n')

    def _write_type_alias_initializer(self, symbol: Symbol, tree: Tree):
        self._use_any()

        initializer = self._initialization_chain(None, tree)

        self._body.write(f'def {to_flat_name(symbol)}__from_json(data: {_T_ANY}) -> {symbol}:\n')

        if initializer:
            self._body.write(f'{_TAB}return {initializer}(data)\n')

        else:
            self._body.write(f'{_TAB}return data\n')

    def write_type_alias(self, symbol: Symbol, tree: Tree):
        """Write a type alias."""

        self._find_externals(tree)

        type_hint = self._type_hint_expression(tree)

        self._body.write(f'{symbol} = {type_hint}\n')
        self._body.write(f'\n')

        self._write_type_alias_validator(symbol, tree)
        self._body.write(f'\n')

        self._write_type_alias_initializer(symbol, tree)
        self._body.write(f'\n')

    #
    # General dump
    #
    def dump(self):
        return '\n'.join([
            self._dump_header(),
            self._body.getvalue()
        ])

def generate_code(module: Mapping[Symbol, Tree]) -> str:

    writer = _Writer()

    for symbol, tree in module.items():
        if isinstance(tree, ClassFields):
            writer.write_class_definition(symbol, tree.fields)
        else:
            writer.write_type_alias(symbol, tree)

    return writer.dump()
