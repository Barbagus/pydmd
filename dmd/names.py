# Copyright 2021 Etienne Zind
#
# This file is part of PyDMD.
#
# PyDMD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDMD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDMD.  If not, see <https://www.gnu.org/licenses/>.

"""
PyDMD (python declarative module definition) -- names utils module

To be clear(er than regex):

    - thisIsAPropertyName ("camel" case with lower first char)

    - ThisIaASymbol ("camel" case with upprt first char)

    - this_is_a_flat_name
"""

import re

SYMBOL_PATTERN = re.compile(r'^[A-Z][a-zA-Z0-9]*$')
FLAT_PATTERN = re.compile(r'^[a-z][a-z0-9]*(_[a-z0-9]+)*$')
TO_FLAT_SUB = re.compile(r'(?<!^)(?=[A-Z0-9])')

def is_symbol(name: str):
    return SYMBOL_PATTERN.match(name) is not None

def is_flat_name(name: str):
    return FLAT_PATTERN.match(name) is not None

def capitalize(name: str):
    """Make first char upper case."""
    return name[0].upper() + name[1:]

def flat_name_to_symbol(name: str):
    return ''.join(capitalize(word) for word in name.split('_'))

def to_flat_name(name: str):
    flat_name = TO_FLAT_SUB.sub('_', name).lower()
    if not is_flat_name(flat_name):
        raise ValueError()
    return flat_name
