# Copyright 2021 Etienne Zind
#
# This file is part of PyDMD.
#
# PyDMD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDMD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDMD.  If not, see <https://www.gnu.org/licenses/>.

"""
PyDMD (python declarative module definition) package
"""

from collections.abc import Iterable
from pathlib import Path
from typing import Union

from .generate import generate_code
from .input import InDocument
from .link import link_modules
from .parse import parse_input


def compile_dmds(dmds: Iterable[tuple[str, InDocument]], output_dir: Union[str, Path]):
    """Compile a (dependent) group fo DMDs."""

    if not isinstance(output_dir, Path):
        output_dir = Path(output_dir)

    modules = {
        module_name: parse_input(dmd)
        for module_name, dmd in dmds
    }

    link_modules(modules)

    for module_name, module in modules.items():
        code = generate_code(module)
        output_dir.joinpath(module_name).with_suffix('.py').write_text(code)

def compile_dmd(module_name: str, dmd: InDocument, output_dir: Union[str, Path]):
    """Compile one DMD."""

    if not isinstance(output_dir, Path):
        output_dir = Path(output_dir)

    module = parse_input(dmd)
    # link_modules() handles internal references
    link_modules({module_name: module})
    code = generate_code(module)

    output_dir.joinpath(module_name).with_suffix('.py').write_text(code)

__all__ = ('compile_dmd', 'compile_dmds')
