# Copyright 2021 Etienne Zind
#
# This file is part of PyDMD.
#
# PyDMD is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyDMD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyDMD.  If not, see <https://www.gnu.org/licenses/>.

"""
PyDMD (python declarative module definition) -- input parsing module
"""

from __future__ import annotations

from collections import OrderedDict
from collections.abc import Mapping
from typing import cast, get_args

from .exceptions import DMDParseException
from .input import InDocument, InMapping, InNode
from .names import flat_name_to_symbol, is_symbol, to_flat_name
from .tree import (ArrayOf, ClassField, ClassFields, EnumValue, NullableValue,
                   OptionalValue, Reference, ScalarType, ScalarValue, Symbol,
                   Tree)

_SCALAR_TYPES = get_args(get_args(ScalarType)[0])

def _parse_ref_node(node: InMapping) -> Tree:

    module_name = node.get('$module')
    if module_name is not None:
        if not isinstance(module_name, str):
            raise DMDParseException(f'$module not a string: {type(module_name)}')

    referred_symbol = node['$ref']
    if not isinstance(referred_symbol, str):
        raise DMDParseException(f'$ref is not a string: {type(referred_symbol)}')

    return Reference(referred_symbol, module_name, None)

def _parse_scalar_node(node: InMapping) -> Tree:
    scalar_type = node['$scalar']

    if not isinstance(scalar_type, str):
        raise DMDParseException(f'$scalar is not a string: {type(scalar_type)}')

    if scalar_type not in _SCALAR_TYPES:
        raise DMDParseException(f'Invalid $scalar value: {scalar_type!r} ({_SCALAR_TYPES!r})')

    return ScalarValue(cast(ScalarType, scalar_type))

def _parse_enum_node(node: InMapping) -> Tree:
    items = node['$enum']

    if not isinstance(items, list):
        raise DMDParseException(f'$enum is not a sequence: {type(items)}')

    for item in items:
        if not isinstance(item, str):
            raise DMDParseException(f'$enum item is not a string: {type(items)}')

    items = cast(list[str], items)

    if len(items) != len(set(items)):
            raise DMDParseException(f'$enum has duplicates: {sorted(items)!r}')

    return EnumValue(items)

def _parse_array_node(node: InMapping, symbol: Symbol, defs: dict[Symbol, Tree]) -> Tree:
    element_node = node['$array']
    element_token = _parse_node(element_node, symbol + 'Item', defs)
    return ArrayOf(element_token)

def _parse_object_node(node: InMapping, symbol: Symbol, defs: dict[Symbol, Tree]) -> Tree:
    properties = node['$object']

    if not isinstance(properties, dict):
        raise DMDParseException(f'$object is not a mapping: {type(properties)}')

    fields: list[ClassField] = []
    flats: set[str] = set()

    for p_name, p_node in properties.items():
        optional = p_name.endswith('?')
        if optional:
            p_name = p_name[:-1]

        try:
            p_flat = to_flat_name(p_name)
        except ValueError:
            raise DMDParseException(f'Could not translate {p_name!r} to flat name.')

        if p_flat in flats:
            raise DMDParseException(f'Flattening name {p_name!r} resulted into duplicate $object member.')

        p_tree = _parse_node(p_node, symbol + flat_name_to_symbol(p_flat), defs)
        if optional:
            p_tree = OptionalValue(p_tree)

        fields.append(
            ClassField(p_flat, p_tree, p_name)
        )

    if symbol in defs:
        raise DMDParseException(f'Symbol collision: {symbol!r}')

    defs[symbol] = ClassFields(fields)
    return Reference(symbol, None, None)

def _to_mapping(node: InNode) -> InMapping:
    # nodes may be string as scalar abreviations, otherwise they must be dicts

    if isinstance(node, dict):
        return node

    if isinstance(node, str):
        return { '$scalar': node }

    raise DMDParseException(f'Invalid node type: {type(node)}')

_SHARED_PROPERTIES = { '$nullable' }

def _parse_node(node: InNode, symbol: Symbol, defs: dict[Symbol, Tree]) -> Tree:
    # switch over node type

    node = _to_mapping(node)

    # workout node's "signature"
    sig = tuple(
        sorted([
            k for k in node.keys() if k not in _SHARED_PROPERTIES
        ])
    )

    if sig == ('$ref',) or sig == ('$module', '$ref'):
        token = _parse_ref_node(node)

    elif sig == ('$scalar',):
        token = _parse_scalar_node(node)

    elif sig == ('$enum',):
        token = _parse_enum_node(node)

    elif sig == ('$array',):
        token = _parse_array_node(node, symbol, defs)

    elif sig == ('$object',):
        token = _parse_object_node(node, symbol, defs)

    else:
        raise DMDParseException(f'Invalid node signature: {sig!r}')

    nullable = node.get('$nullable', False)
    if not isinstance(nullable, bool):
        raise DMDParseException(f'$nullable property is not boolean: {type(nullable)}')

    if nullable:
        token = NullableValue(token)

    return token

def parse_input(input: InDocument) -> Mapping[str, Tree]:

    if not isinstance(input, dict):
        raise DMDParseException(f'Input document MUST be a mapping: {type(input)}')

    # we use an ordered dict to get a somehow logical order
    defs: OrderedDict[Symbol, Tree] = OrderedDict()

    for symbol in sorted(input):
        node = input[symbol]
        if not is_symbol(symbol):
            raise DMDParseException(f'Invalid symbol: {symbol!r}')

        tree = _parse_node(node, symbol, defs)

        if isinstance(tree, Reference) and tree.symbol == symbol and tree.module_name is None:
            pass
        elif symbol in defs:
            raise DMDParseException(f'Symbol collision: {symbol!r}')
        else:
            defs[symbol] = tree

    return defs
