#
# Code not licensed
#

from typing import Any as any
from typing import cast as cast

def _as_optional(chain: any):
    def inner(data: any):
        return None if data is None else chain(data)

    return inner

as_optional = _as_optional

def _as_nullable(chain: any):
    def inner(data: any):
        return None if data is None else chain(data)

    return inner

as_nullable = _as_nullable

def _as_list(chain: any):
    def inner(data: any):
        return [chain(e) for e in data]

    return inner

as_list = _as_list

def _is_enum(items: list[str]):
    def inner(data: any, path: str):
        if not isinstance(data, str):
            raise TypeError(f'{path or "/"} not a string: {type(data)}')

        if data not in items:
            raise ValueError(f'{path or "/"} invalid value: {data!r}')

    return inner

is_enum = _is_enum

def _is_list(chain: any):
    def inner(data: any, path: str):
        if not isinstance(data, list):
            raise TypeError(f'{path or "/"} not a list: {type(data)}')

        for i, e in enumerate(cast(list[any], data)):
            chain(e, path + '/' + str(i))

    return inner

is_list = _is_list

def _is_optional(chain: any):
    def inner(data: any, path: str):
        if data is not None:
            chain(data, path)

    return inner

is_optional = _is_optional

def _is_nullable(chain: any):
    def inner(data: any, path: str):
        if data is not None:
            chain(data, path)

    return inner

is_nullable = _is_nullable

def _is_scalar(scalar: any, name: str):
    def inner(data: any, path: str):
        if not isinstance(data, scalar):
            raise TypeError(f'{path or "/"} not a {name}: {type(data)}')

    return inner

is_scalar = _is_scalar

def _is_mapping(data: any, fields: set[str], path: str):
    if not isinstance(data, dict):
        raise TypeError(f'{path or "/"} not a mapping: {type(data)}')

    data = cast(dict[str, any], data)
    keys = set(data)
    if not keys.issubset(fields):
        raise ValueError(f'{path or "/"} extra members: {keys - fields}')

is_mapping = _is_mapping
